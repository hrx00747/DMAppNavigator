//
//  DMSampleMenu.m
//  DMAppFramework
//
//  Created by chenxinxin on 15/11/9.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "DMSampleMenu.h"

@interface DMSampleMenu ()

@end

@implementation DMSampleMenu


- (IBAction)onBasicNaviClicked:(id)sender {
//    [self.navigator forward:@"app://category?lvalue=110&bvalue=1&cvalue='0'"];
//    [self.navigator forward:@"app://DMPageOne"];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"DMWebPageThree" ofType:@"html"];
    NSString* url = [NSString stringWithFormat:@"file://%@",path];
    [self.navigator forward:url];
}




@end
