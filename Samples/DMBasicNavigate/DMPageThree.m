//
//  DMPageThree.m
//  DMAppFramework
//
//  Created by chris on 15/11/19.
//  Copyright © 2015年 dmall. All rights reserved.
//

#import "DMPageThree.h"
#import <JavaScriptCore/JavaScriptCore.h>

@interface DMPageThree ()

@property(nonatomic, weak)  IBOutlet    UILabel     *titleLabel;

@end

@implementation DMPageThree

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.titleLabel.text = self.content;
    
}


- (IBAction)onClickBack:(id)sender{
    
    [self.navigator backward];
}

- (IBAction)onClickNext:(id)sender{
    
    NSString *url = @"http://www.baidu.com";
    [self.navigator forward:url];
    
}

@end
