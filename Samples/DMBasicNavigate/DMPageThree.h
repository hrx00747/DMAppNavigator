//
//  DMPageThree.h
//  DMAppFramework
//
//  Created by chris on 15/11/19.
//  Copyright © 2015年 dmall. All rights reserved.
//

#import "DMPage.h"

@interface DMPageThree : DMPage

@property(nonatomic, copy)  NSString         *content;

@end
