//
//  DMPageTwo.h
//  DMAppFramework
//
//  Created by chenxinxin on 15/10/27.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "DMPage.h"

@interface DMPageTwo : DMPage

@property (retain,nonatomic) NSString* title;
@property (assign,nonatomic) int value;
@end
