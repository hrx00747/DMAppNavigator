//
//  DMPageOne.m
//  DMAppFramework
//
//  Created by chenxinxin on 15/10/27.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "DMPageOne.h"
#import "DMPageThree.h"
#import <JavaScriptCore/JavaScriptCore.h>


@interface DMPageOne ()
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end

@implementation DMPageOne

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    NSURL *url = [[NSBundle mainBundle] URLForResource:@"testgif" withExtension:@"gif"];
//    NSData *data = [NSData dataWithContentsOfURL:url];
//    [self.gifView loadFromData:data];
//    
//    [self.gifView playOnce];
//    [self javascriptCoreText];
}


-(void) setButton:(UIButton *)button {
    button.layer.cornerRadius = 8;
    button.layer.masksToBounds = YES;
    self->_button = button;
}
- (IBAction)onBackClicked:(id)sender {
    [self.navigator backward];
}





- (IBAction)onClicked:(id)sender {
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"DMWebPageThree" ofType:@"html"];
//    NSString* url = [NSString stringWithFormat:@"file://%@",path];
//    [self.navigator forward:url callback:^(NSDictionary *dict) {
//        NSLog(@"page callback");
//    }];
    
//    [self.navigator forward:@"app://DMPageThree?content=这是一个内容"];
    
//    [self.navigator forward:@"up://DMListViewTest.xml"];
    
    [self.navigator forward:@"rn://TestPage"];
}

- (void) javascriptCoreText{
    
    JSContext *context = [[JSContext alloc] init];
    
    context[@"test"] = ^(NSString *name){
        NSLog(@"hello %@",name);
    };
    
    [context evaluateScript:@"test('hrx')"];
}


@end
