//
//  DMPageTwo.m
//  DMAppFramework
//
//  Created by chenxinxin on 15/10/27.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "DMPageTwo.h"
#import "DMUrlEncoder.h"

@interface DMPageTwo ()
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation DMPageTwo

-(void) setButton:(UIButton *)button {
    button.layer.cornerRadius = 8;
    button.layer.masksToBounds = YES;
    self->_button = button;
}
- (IBAction)onButtonClicked:(id)sender {
    [self.navigator backward:@"param=测试中文&param2=abc"];
}

-(void) pageWillBeShown {
    [super pageWillBeShown];
    self.label.text = [NSString stringWithFormat:@"title:%@ value:%d",self.title,self.value];
}

@end
