//
//  DMWebPage.m
//  DMAppFramework
//
//  Created by chenxinxin on 15/10/27.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "DMWebPage.h"
#import "DMWebBridge.h"

@interface DMWebPage () <UIWebViewDelegate>

@property (nonatomic, strong)   DMWebBridge     *bridge;
@end

@implementation DMWebPage

-(NSString*) evaluateScript:(NSString*) script {
    
    return [self.webView stringByEvaluatingJavaScriptFromString:script];
}

- (DMWebBridge*) bridge{
    
    if(!_bridge){
        _bridge = [DMWebBridge webBridge];
        _bridge.jsPage = self.webView;
        _bridge.navigator = self.navigator;
    }
    return _bridge;
}


-(UIWebView*) webView {
    if (self->_webView == nil) {
        self->_webView = [[UIWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self->_webView.backgroundColor = [UIColor greenColor];
    }
    return self->_webView;
}

-(void) loadView {
    self.view = self.webView;
    self.webView.delegate = self;
    [self.bridge registBridgeScript:self.webView];
   
}




-(void) pageDestroy {
    [super pageDestroy];
    [self.webView stopLoading];
    self.webView = nil;
}

-(void) pageWillForwardToMe {
    [super pageWillForwardToMe];
    NSString* pageUrl = self.pageUrl;
    NSURL* url = nil;
    if ([pageUrl rangeOfString:@"file://"].location == 0) {
        NSString*   filePath = [pageUrl substringFromIndex:7];
        NSString* fileFolder = [filePath substringToIndex:
                                    (filePath.length-filePath.lastPathComponent.length-1)];
        NSURL*       context = [NSURL fileURLWithPath:fileFolder];
        
        [self.webView loadHTMLString:[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil] baseURL:context];
    } else {
        url = [NSURL URLWithString:pageUrl];
        NSURLRequest* request = [NSURLRequest requestWithURL:url] ;
        [self.webView loadRequest:request];
    }
}

@end
