//
//  DMRNPage.m
//  DMAppNavigator
//
//  Created by chris on 16/7/11.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import "DMRNPage.h"
#import "RCTRootView.h"
#import "DMRNBridge.h"

@interface DMRNPage ()

@property(nonatomic, strong)    RCTRootView         *rootView;
@property(nonatomic, strong)    DMRNBridge          *rnBridge;

@end

@implementation DMRNPage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}


- (RCTRootView*) rootView{
    
    if(!_rootView){
        _rootView = [[RCTRootView alloc] initWithBridge:self.rnBridge.innerBridge moduleName:self.pageName initialProperties:nil];
        _rootView.frame = self.view.bounds;
        
    }
    
    return _rootView;
}

- (DMRNBridge *) rnBridge{
    
    if(!_rnBridge){
        _rnBridge = [DMRNBridge rnBridge];
    }
    
    return _rnBridge;
}


- (void) pageWillForwardToMe{
    
    [self.view addSubview:self.rootView];
}





@end
