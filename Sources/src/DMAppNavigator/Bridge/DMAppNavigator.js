(function(){
    if("undefined" == typeof com){
        com = {};
    }
    if("undefined" == typeof com.dmall){
        com.dmall = {};
    }

    com.dmall.Navigator = {

        forward: function(url,callback){
            if(callback){
                com.dmall.Bridge.callbackFun = callback;
            }
            else{
                com.dmall.Bridge.callbackFun = function(){};
            }
            var args = new Array(url);
            com.dmall.Bridge.callNative('','forward',args);
        },
            
        backward: function(param){
            var args = new Array(param);
            com.dmall.Bridge.callNative('','backward',args);
        },
            
        callback: function(param){
            var args = new Array(param);
            com.dmall.Bridge.callNative('','callback',args);
        },
            
        pushFlow: function(){
            var args = new Array();
            com.dmall.Bridge.callNative('','pushFlow',args);
        },
            
        popFlow: function(param){
            var args = new Array(param);
            com.dmall.Bridge.callNative('','popFlow',args);
        }
        
    };

    com.dmall.Bridge = {
        
        callbackFun:{},
        
        callJS: function(param){
            var map = {};
            var eles = param.split("&");
            for(var i in eles) {
                var obj = eles[i];
                var objEles = obj.split("=");
                var key = decodeURI(objEles[0]);
                var value = decodeURI(objEles[1]);
                map[key] = value;
            }
            com.dmall.Bridge.callbackFun(map);
        },
        
        callNative: function(module,method,args){
            
            var request = new XMLHttpRequest();
            var argsText = "[";
            for(var index=0;index<args.length;index++){
                argsText += "\""+args[index]+"\"";
                if(index<args.length-1){
                    argsText += ",";
                }
            }
            argsText += "]";
 
            var param = "{\"module\":\""+module+"\",\"method\":\""+method+"\",\"args\":"+argsText+"}";
            
            request.open('HEAD', "/!lightapp/"+encodeURI(param), false);
            request.send(null);
            
        }
        
    };

})();