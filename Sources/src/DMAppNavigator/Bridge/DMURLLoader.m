//
//  DMURLLoader.m
//  DMAppNavigator
//
//  Created by chris on 16/7/8.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import "DMURLLoader.h"

#define URL_PREFEX  @"/!lightapp/"

@implementation DMURLLoader

+ (void) initialize{
    
    [NSURLProtocol registerClass:[self class]];
}

#pragma mark - NSURLProtocal

+ (NSURLRequest*) canonicalRequestForRequest:(NSURLRequest *)request{
    
    return request;
}

+ (BOOL) requestIsCacheEquivalent:(NSURLRequest *)a toRequest:(NSURLRequest *)b{
    
    return [super requestIsCacheEquivalent:a toRequest:b];
}


+ (BOOL) canInitWithRequest:(NSURLRequest *)request{
    
    NSURL *url = request.URL;
    if(url){
        NSRange range = [url.absoluteString rangeOfString:URL_PREFEX];
        if(range.location!=NSNotFound){
            return YES;
        }
    }
    
    return NO;
}



-(instancetype) initWithRequest:(NSURLRequest *)request cachedResponse:(NSCachedURLResponse *)cachedResponse client:(id<NSURLProtocolClient>)client {
    if(self = [super initWithRequest:request cachedResponse:cachedResponse client:client]){
    }
    return self;
}

- (void) startLoading{
    
    NSString *responseData  = nil;
    NSString* url           = self.request.URL.absoluteString;
    
    if (url == nil) {
        return;
    }
    
    NSRange range           = [url rangeOfString:URL_PREFEX];
    
    
    if (range.location != NSNotFound) {
        
        NSString *content = [[url substringFromIndex:range.location+range.length] stringByRemovingPercentEncoding];
        
        NSError* error;
        NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:[content dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (dic != nil) {
//            NSString* module = [dic objectForKey:@"module"];
            NSString* method = [dic objectForKey:@"method"];
            NSArray*  param  = [dic objectForKey:@"args"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[DMWebBridge webBridge] invokeFromJavaScript:method withParam:param];
            });
            
        }
    }
    
    NSData* data = [@"[[nil]]" dataUsingEncoding:NSUTF8StringEncoding];
    if(responseData != nil) {
        data = [responseData dataUsingEncoding:NSUTF8StringEncoding];
    }
    NSURLResponse* response = [[NSURLResponse alloc] initWithURL:self.request.URL MIMEType:@"text/plain" expectedContentLength:data.length textEncodingName:@"UTF-8"];
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
    [self.client URLProtocol:self didLoadData:data];
    [self.client URLProtocolDidFinishLoading:self];
    
}

- (void) stopLoading{
    
    
}

@end
