//
//  DMWebBridge.m
//  DMAppNavigator
//
//  Created by chris on 16/7/7.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import "DMWebBridge.h"
#import <objc/runtime.h>
#import "DMStringUtils.h"
#import "DMURLLoader.h"
#import "DMUrlEncoder.h"

@interface DMWebBridge ()

@property(nonatomic, strong)    NSMutableDictionary     *methodMap;
@property(nonatomic, strong)    DMURLLoader             *urlLoader;

@end

union ArgUnion {
    char charValue;
    unsigned char unsignedChar;
    short shortValue;
    unsigned short unsignedShortValue;
    long longValue;
    unsigned long unsignedLongValue;
    int intValue;
    unsigned int unsignedIntValue;
    float floatValue;
    double doubleValue;
    BOOL boolValue;
    long long longLongValue;
    unsigned long long unsignedLongLongValue;
};

@implementation DMWebBridge

static DMWebBridge  *s_bridge = nil;

#pragma mark - inherit from supper class
- (void) forward:(NSString *)url{
    
    [self.navigator forward:url callback:^(NSDictionary *param) {
        NSString* str = [DMUrlEncoder encodeParams:param];
        [self.jsPage stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"com.dmall.Bridge.callJS(\"%@\")",str]];
    }];
}

+ (instancetype) webBridge{
    if(!s_bridge){
        s_bridge = [[DMWebBridge alloc] init];
    }
    return s_bridge;
}

- (instancetype) init{
    
    if(self=[super init]){
        self.methodMap = [NSMutableDictionary dictionary];
        self.urlLoader = [[DMURLLoader alloc] init];
    }
    
    return self;
}

- (void) registBridgeScript:(UIWebView*)webView{
    
    //Load DMAppNavigator.js
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DMAppNavigator" ofType:@"js"];
    NSString *scripts = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    
    [webView stringByEvaluatingJavaScriptFromString:scripts];
    
    
    //init method map
    [self collectJSExportMethodsWithCallback:^(SEL selector) {
        
        NSString *ocMethodName = NSStringFromSelector(selector);
        NSString *jsMethodName = [self extractMehtodNameFromSelector:selector];
        
        [self.methodMap setObject:ocMethodName forKey:jsMethodName];
        
    }];
    
}

- (NSString*) invokeFromJavaScript:(NSString *)methodName withParam:(NSArray *)param{
    
    SEL selector = [self selectorFromJSMethodName:methodName];
    NSMethodSignature *methodSign = [[self class] instanceMethodSignatureForSelector:selector];
    NSInvocation* invo = [NSInvocation invocationWithMethodSignature:methodSign];
    [invo setSelector:selector];
    NSMutableArray* buffer = [[NSMutableArray alloc] init];
    unsigned long argCount = methodSign.numberOfArguments - 2;
    for(int i = 0; i < argCount ; i++) {
        unsigned int index = i + 2;
        const char * argType = [methodSign getArgumentTypeAtIndex:index];
        NSString* argValue = [param[i] stringByRemovingPercentEncoding];
        if (strcmp(argType, "@") == 0) {
            NSString* arg = argValue;
            [invo setArgument:&arg atIndex:index];
            [buffer addObject:arg];
        } else {
            [buffer addObject:argValue];
            union ArgUnion argUnion = [self argUnionFromString:argValue withType:argType];
            [invo setArgument:&argUnion atIndex:index];
        }
    }
    
    [invo invokeWithTarget:self];
    
    const char* methodRetType = [methodSign methodReturnType];
    if (strcmp(methodRetType, "@") == 0) {
        void* retObject;
        [invo getReturnValue:&retObject];
        NSString* ret = (__bridge NSString*)retObject;
        return ret;
    }
    else if(strcmp(methodRetType,"v") != 0){
        union ArgUnion argUnion;
        [invo getReturnValue:&argUnion];
        return [self stringFromArgUnion:argUnion withType:methodRetType];
    }
    return @"";
    
    
}


- (SEL) selectorFromJSMethodName:(NSString*)jsMethodName{
    
    NSString *ocMehtodName = [self.methodMap objectForKey:jsMethodName];
    return NSSelectorFromString(ocMehtodName);
    
}



- (void) collectJSExportMethodsWithCallback:(void (^) (SEL selector))collect{
    
    unsigned int protocolCount = 0;
    Protocol *__unsafe_unretained* protocols = class_copyProtocolList([self class], &protocolCount);
    
    for (int i = 0; i < protocolCount; i++) {
        Protocol* protocol = protocols[i];
        
        if (!protocol_conformsToProtocol(protocol, @protocol(JSExport))) {
            continue;
        }
        
        unsigned int methodCount = 0;
        struct objc_method_description * methods = protocol_copyMethodDescriptionList(protocol,YES,YES,&methodCount);
        for (int j = 0 ; j < methodCount; j++) {
            SEL sel = methods[j].name;
            collect(sel);
        }
        free(methods);
    }
    free(protocols);
    
}

- (NSString*) extractMehtodNameFromSelector:(SEL)selector{
    
    NSString *methodName = NSStringFromSelector(selector);
    NSMutableString *buffer = [[NSMutableString alloc] init];
    
    NSArray *names = [methodName componentsSeparatedByString:@":"];
    for(int i=0;i<names.count;i++){
        if(i==0){
            [buffer appendString:names[i]];
        }
        else{
            [buffer appendString:[DMStringUtils firstToUpper:names[i]]];
        }
    }
    
    return buffer;
}

- (NSString*) stringFromArgUnion:(union ArgUnion)arg withType:(const char*)type{
    
    if (strcmp(type, "c") == 0) {
        return [NSString stringWithFormat:@"%c",arg.charValue];
    }
    if (strcmp(type, "i") == 0) {
        return [NSString stringWithFormat:@"%d",arg.intValue];
    }
    if (strcmp(type, "I") == 0) {
        return [NSString stringWithFormat:@"%u",arg.unsignedIntValue];
    }
    if (strcmp(type, "s") == 0) {
        return [NSString stringWithFormat:@"%d",arg.shortValue];
    }
    if (strcmp(type, "S") == 0) {
        return [NSString stringWithFormat:@"%u",arg.unsignedShortValue];
    }
    if (strcmp(type, "l") == 0) {
        return [NSString stringWithFormat:@"%ld",arg.longValue];
    }
    if (strcmp(type, "L") == 0) {
        return [NSString stringWithFormat:@"%lu",arg.unsignedLongValue];
    }
    if (strcmp(type, "f") == 0) {
        return [NSString stringWithFormat:@"%f",arg.floatValue];
    }
    if (strcmp(type, "d") == 0) {
        return [NSString stringWithFormat:@"%f",arg.doubleValue];
    }
    if (strcmp(type, "B") == 0) {
        return [NSString stringWithFormat:@"%d",arg.boolValue];
    }
    if (strcmp(type, "q") == 0) {
        return [NSString stringWithFormat:@"%lld",arg.longLongValue];
    }
    if (strcmp(type, "Q") == 0) {
        return [NSString stringWithFormat:@"%llu",arg.unsignedLongLongValue];
    }
    return @"";
}

- (union ArgUnion) argUnionFromString:(NSString*)value withType:(const char*)type{
    
    
    union ArgUnion ret;
    if (strcmp(type, "c") == 0) {
        ret.charValue = [value characterAtIndex:0];
        if ([@"true" isEqualToString:value] || [@"false" isEqualToString:value]) {
            ret.boolValue = [value boolValue];
        }
        return ret;
    }
    if (strcmp(type, "i") == 0) {
        ret.intValue = [value intValue];
        return ret;
    }
    if (strcmp(type, "I") == 0) {
        ret.unsignedIntValue = (unsigned int)[value intValue];
        return ret;
    }
    if (strcmp(type, "s") == 0) {
        ret.shortValue = [value intValue];
        return ret;
    }
    if (strcmp(type, "S") == 0) {
        ret.unsignedShortValue = [value intValue];
        return ret;
    }
    if (strcmp(type, "l") == 0) {
        ret.longValue = [value longLongValue];
        return ret;
    }
    if (strcmp(type, "L") == 0) {
        ret.unsignedLongLongValue = [value longLongValue];
        return ret;
    }
    if (strcmp(type, "f") == 0) {
        ret.floatValue = [value floatValue];
        return ret;
    }
    if (strcmp(type, "d") == 0) {
        ret.doubleValue = [value doubleValue];
        return ret;
    }
    if (strcmp(type, "B") == 0) {
        ret.boolValue = [value boolValue];
        return ret;
    }
    if (strcmp(type, "q") == 0) {
        ret.longLongValue = [value longLongValue];
        return ret;
    }
    
    return ret;
}


@end
