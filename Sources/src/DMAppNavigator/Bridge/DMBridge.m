//
//  DMBridge.m
//  DMAppNavigator
//
//  Created by chris on 16/7/7.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import "DMBridge.h"
#import "DMUrlEncoder.h"
#import "DMPage.h"

@implementation DMBridge

- (instancetype) init{
    if(self=[super init]){
        self.navigator = [DMNavigator getInstance];
    }
    return self;
}

- (void) forward:(NSString *)url{
    
    [self.navigator forward:url];
}

- (void) backward:(NSString *)param{
    
    [self.navigator backward:param];
}

- (void) callback:(NSString *)param{
    
    [self.navigator callback:param];
}

- (void) pushFlow{
    
    [self.navigator pushFlow];
}

- (void) popFlow:(NSString *)param{
    
    [self.navigator popFlow:param];
}



@end
