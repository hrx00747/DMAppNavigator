//
//  DMRNBridge.h
//  DMAppNavigator
//
//  Created by chris on 16/7/11.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import "DMBridge.h"
#import "RCTBridge.h"
#import "RCTBridgeDelegate.h"
#import "RCTBridge+Private.h"
#import "RCTBridgeModule.h"


@interface DMRNBridge : DMBridge<RCTBridgeDelegate,RCTBridgeModule>

@property(nonatomic, strong)    RCTBridge       *innerBridge;


+ (instancetype) rnBridge;

@end
