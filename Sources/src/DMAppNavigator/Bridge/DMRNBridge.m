//
//  DMRNBridge.m
//  DMAppNavigator
//
//  Created by chris on 16/7/11.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import "DMRNBridge.h"
#import "DMUrlEncoder.h"

@implementation DMRNBridge


RCT_EXPORT_MODULE(DMNavigator)

static DMRNBridge   *s_bridge = nil;

+ (instancetype) rnBridge{
    
    if(!s_bridge){
        s_bridge = [[DMRNBridge alloc] init];
    }
    
    return s_bridge;
}

- (instancetype) init{
    
    if(self=[super init]){

    }
    return self;
}


- (RCTBridge*) innerBridge{
    
    if(!_innerBridge){
        
        _innerBridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:nil];
        
    }
    return _innerBridge;
}

RCT_EXPORT_METHOD(forward:(NSString*)url callback:(RCTResponseSenderBlock) callback){
    
    
    void (^nativeCallback)(NSDictionary*) = ^(NSDictionary *param){
        NSString* str = [DMUrlEncoder encodeParams:param];
        callback(@[[NSNull null],str]);
        
    };
    [self.navigator forward:url callback:nativeCallback];
}


RCT_EXPORT_METHOD(backward:(NSString*)param){
    
    [self.navigator backward:param];
}

RCT_EXPORT_METHOD(callback:(NSString*)param){
    
    [self.navigator callback:param];
}


RCT_EXPORT_METHOD(pushFlow){
    
    [self.navigator pushFlow];
}

RCT_EXPORT_METHOD(popFlow:(NSString*)param){
    
    [self.navigator popFlow:param];
}


#pragma mark - RCTBridgeDelegate
- (NSURL*) sourceURLForBridge:(RCTBridge *)bridge{
    
    NSURL *bundleURL = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    return bundleURL;
    
}

#pragma mark - RCTBridgeModule
- (dispatch_queue_t) methodQueue{
    
    return dispatch_get_main_queue();
}

@end
