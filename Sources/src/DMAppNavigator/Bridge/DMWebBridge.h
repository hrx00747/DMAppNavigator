//
//  DMWebBridge.h
//  DMAppNavigator
//
//  Created by chris on 16/7/7.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import "DMBridge.h"
#import <JavaScriptCore/JavaScriptCore.h>

@protocol DMWebBridgeExport <JSExport>

- (void) forward:(NSString*)url;

- (void) backward:(NSString*)param;

- (void) callback:(NSString*)param;

- (void) pushFlow;

- (void) popFlow:(NSString*)param;


@end


@interface DMWebBridge : DMBridge <DMWebBridgeExport>

@property(nonatomic, weak)  UIWebView       *jsPage;


+ (instancetype) webBridge;


- (void) registBridgeScript:(UIWebView*)webView;

-(NSString*) invokeFromJavaScript: (NSString*) methodName withParam:(NSArray*) param;

@end
