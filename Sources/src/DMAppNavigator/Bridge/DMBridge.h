//
//  DMBridge.h
//  DMAppNavigator
//
//  Created by chris on 16/7/7.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMNavigator.h"

@interface DMBridge : NSObject

@property(weak, nonatomic)  DMNavigator     *navigator;

- (void) forward:(NSString*)url;

- (void) backward:(NSString*)param;

- (void) callback:(NSString*)param;

- (void) pushFlow;

- (void) popFlow:(NSString*)param;




@end
