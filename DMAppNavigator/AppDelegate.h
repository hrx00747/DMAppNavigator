//
//  AppDelegate.h
//  DMAppNavigator
//
//  Created by chris on 16/7/7.
//  Copyright © 2016年 dmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

